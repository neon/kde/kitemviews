Source: kitemviews
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 3.0~),
               debhelper (>= 11~),
               doxygen,
               extra-cmake-modules (>= 5.51.0~),
               graphviz,
               libqt5sql5-sqlite:native,
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               qtbase5-dev (>= 5.8.0~),
               qttools5-dev (>= 5.4),
               qttools5-dev-tools (>= 5.4)
Standards-Version: 4.1.4
Homepage: https://projects.kde.org/projects/frameworks/kitemviews
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kitemviews
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kitemviews.git

Package: libkf5itemviews-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Qt library with additional widgets for ItemModels
 A Qt library which contains additional widgets for ItemModels, such as
 grouping into categories and filtering
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains the translations.

Package: libkf5itemviews-dev
Architecture: any
Section: libdevel
Depends: libkf5itemviews5 (= ${binary:Version}),
         qtbase5-dev (>= 5.8.0~),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: kio-dev (<< 5.28),
        libkf5iconthemes-dev (<< 5.51),
        libkf5kcmutils-dev (<< 5.51),
        libkf5kio-dev (<< 5.51),
        libkf5xmlgui-dev (<< 5.51)
Replaces: libkf5itemviews-doc (<< 5.61.90-0)
Recommends: libkf5itemviews-doc (= ${source:Version})
Description: Qt library with additional widgets for ItemModels
 A Qt library which contains additional widgets for ItemModels, such as
 grouping into categories and filtering
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains the development files.

Package: libkf5itemviews-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Qt library with additional widgets for ItemModels (documentation)
 A Qt library which contains additional widgets for ItemModels, such as
 grouping into categories and filtering
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains the qch documentation files.
Section: doc

Package: libkf5itemviews5
Architecture: any
Multi-Arch: same
Depends: libkf5itemviews-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Qt library with additional widgets for ItemModels
 A Qt library which contains additional widgets for ItemModels, such as
 grouping into categories and filtering
 .
 This package is part of KDE Frameworks 5.
Breaks: kdesignerplugin (<< 5.51),
        kio (<< 5.51),
        kross (<< 5.42),
        libkf5iconthemes5 (<< 5.51),
        libkf5kcmutils5 (<< 5.51),
        libkf5kdelibs4support5 (<< 5.42),
        libkf5kdelibs4support5-bin (<< 5.51),
        libkf5kiofilewidgets5 (<< 5.51),
        libkf5newstuff5 (<< 5.51),
        libkf5peoplewidgets5 (<< 5.51),
        libkf5texteditor5 (<< 5.51),
        libkf5xmlgui5 (<< 5.51)
